#DEPLOY APPLICATION
kubectl apply -f hpa-demo-app.yaml

#CREATE HPA FOR APPLICATION
kubectl apply -f hpa.yaml

#INCREASE THE WORK LOAD ON APPLICATION
kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://hpa-demo-deployment; done"

#CHECK THE HPA STATUS
kubectl get hpa

#CHECK IF REPLICAS COUNT IS INCREASING TO HANDLE THE WORK LOAD
kubectl get hpa hpa-demo-deployment -w

#CHECK THE COUNT OF PODS
kubectl get po
